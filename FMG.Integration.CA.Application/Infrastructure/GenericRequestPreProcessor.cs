﻿using System.Threading;
using System.Threading.Tasks;
using FMG.Integration.CA.Application.Interfaces;
using FMG.Integration.CA.Application.Interfaces.Enumerations;
using MediatR.Pipeline;

namespace FMG.Integration.CA.Application.Infrastructure
{
    public class GenericRequestPreProcessor<TRequest> : IRequestPreProcessor<TRequest>
    {
        private readonly ILogTrace _logTrace;

        public GenericRequestPreProcessor(ILogTrace logTrace)
        {
            _logTrace = logTrace;
        }

        public Task Process(TRequest request, CancellationToken cancellationToken)
        {
            _logTrace.Add(LogLevel.Debug, $"- Starting Up {typeof(TRequest).Name}", new { request });
            return Task.Delay(1);
        }
    }
}
