﻿using System;
using System.Threading;
using System.Threading.Tasks;
using FMG.Integration.CA.Application.Interfaces;
using FMG.Integration.CA.Application.Interfaces.Enumerations;
using MediatR;

namespace FMG.Integration.CA.Application.Infrastructure
{
    public class GenericPipelineBehavior<TRequest, TResponse> :
        IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ILogTrace _logTrace;

        public GenericPipelineBehavior(ILogTrace logTrace)
        {
            _logTrace = logTrace;
        }

        public async Task<TResponse> Handle(TRequest request,
            CancellationToken cancellationToken,
            RequestHandlerDelegate<TResponse> next)
        {
            var startedAt = DateTime.UtcNow;
            _logTrace.Add(LogLevel.Debug, "-- Handling Request", null);
            var response = await next();
            _logTrace.Add(LogLevel.Debug, "-- Finished Request", null, startedAt);

            return response;
        }
    }
}
