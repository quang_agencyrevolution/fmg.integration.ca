﻿using System.Threading;
using System.Threading.Tasks;
using FMG.Integration.CA.Application.Interfaces;
using FMG.Integration.CA.Application.Interfaces.Enumerations;
using MediatR.Pipeline;

namespace FMG.Integration.CA.Application.Infrastructure
{
    public class GenericRequestPostProcessor<TRequest, TResponse> :
        IRequestPostProcessor<TRequest, TResponse>
    {
        private readonly ILogTrace _logTrace;

        public GenericRequestPostProcessor(ILogTrace logTrace)
        {
            _logTrace = logTrace;
        }

        public Task Process(TRequest request, TResponse response, CancellationToken cancellationToken)
        {
            _logTrace.Add(LogLevel.Debug, $"- All Done {typeof(TRequest).Name}", new { request });
            return Task.Delay(1);
        }
    }
}
