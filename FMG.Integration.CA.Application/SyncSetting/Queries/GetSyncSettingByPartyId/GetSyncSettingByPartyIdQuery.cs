﻿using FMG.Integration.CA.Application.SyncSetting.Models;
using MediatR;

namespace FMG.Integration.CA.Application.SyncSetting.Queries.GetSyncSettingByPartyId
{
    public class GetSyncSettingByPartyIdQuery : IRequest<SyncSettingModel>
    {
        public int PartyId { get; set; }
    }
}
