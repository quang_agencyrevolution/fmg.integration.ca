﻿using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Application.Exceptions;
using FMG.Integration.CA.Application.SyncSetting.Models;
using FMG.Integration.CA.Domain.Entities;
using MediatR;

namespace FMG.Integration.CA.Application.SyncSetting.Queries.GetSyncSettingByPartyId
{
    public class GetSyncSettingByPartyIdQueryHandler :
        IRequestHandler<GetSyncSettingByPartyIdQuery, SyncSettingModel>
    {
        private readonly SqlConnection _sqlConnection;

        public GetSyncSettingByPartyIdQueryHandler(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public async Task<SyncSettingModel> Handle(GetSyncSettingByPartyIdQuery request
            , CancellationToken cancellationToken)
        {
            var syncSetting = await _sqlConnection.GetAsync<SyncSettingEntity>(request.PartyId);

            if (syncSetting == null)
                throw new NotFoundException($"Not found SyncSetting for PartyId = {request.PartyId}");

            return new SyncSettingModel
            {
                DailySynced = syncSetting.DailySynced,
                IntegrationType = syncSetting.IntegrationType,
                LastManualSyncDate = syncSetting.LastManualSyncDate,
                LastSyncFinishDate = syncSetting.LastSyncFinishDate,
                LastSyncStartDate = syncSetting.LastSyncStartDate,
                PartyId = syncSetting.PartyId,
                PreviousSyncStartDate = syncSetting.PreviousSyncStartDate,
                RemotePartyId = syncSetting.RemotePartyId,
                SyncStatus = syncSetting.ContactSyncStatus
            };
        }
    }
}
