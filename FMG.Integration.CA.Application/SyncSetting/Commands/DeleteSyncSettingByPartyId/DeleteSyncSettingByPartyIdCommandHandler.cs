﻿using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Domain.Entities;
using MediatR;

namespace FMG.Integration.CA.Application.SyncSetting.Commands.DeleteSyncSettingByPartyId
{
    public class DeleteSyncSettingByPartyIdCommandHandler :
        IRequestHandler<DeleteSyncSettingByPartyIdCommand, Unit>
    {
        private readonly SqlConnection _sqlConnection;

        public DeleteSyncSettingByPartyIdCommandHandler(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public async Task<Unit> Handle(DeleteSyncSettingByPartyIdCommand request, CancellationToken cancellationToken)
        {
            // true if deleted, false if not found
            await _sqlConnection.DeleteAsync(new SyncSettingEntity
            {
                PartyId = request.PartyId
            });

            return Unit.Value;
        }
    }
}
