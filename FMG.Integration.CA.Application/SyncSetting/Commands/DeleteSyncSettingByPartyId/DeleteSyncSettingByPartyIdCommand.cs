﻿using MediatR;

namespace FMG.Integration.CA.Application.SyncSetting.Commands.DeleteSyncSettingByPartyId
{
    public class DeleteSyncSettingByPartyIdCommand : IRequest
    {
        public int PartyId { get; set; }
    }
}
