﻿using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Application.Exceptions;
using FMG.Integration.CA.Application.SyncSetting.Models;
using FMG.Integration.CA.Domain.Entities;
using MediatR;

namespace FMG.Integration.CA.Application.SyncSetting.Commands.UpdateSyncSetting
{
    public class UpdateSyncSettingCommandHandler :
        IRequestHandler<UpdateSyncSettingCommand, SyncSettingModel>
    {
        private readonly SqlConnection _sqlConnection;

        public UpdateSyncSettingCommandHandler(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public async Task<SyncSettingModel> Handle(UpdateSyncSettingCommand request, CancellationToken cancellationToken)
        {
            var syncSettingCurrent = await _sqlConnection.GetAsync<SyncSettingEntity>(request.PartyId);

            if (syncSettingCurrent == null)
            {
                throw new NotFoundException($"Not found SyncSetting for PartyId = {request.PartyId}");
            }

            var syncSetting = new SyncSettingEntity
            {
                DailySynced = request.DailySynced ?? syncSettingCurrent.DailySynced,
                IntegrationType = request.IntegrationType ?? syncSettingCurrent.IntegrationType,
                LastManualSyncDate = request.LastManualSyncDate ?? syncSettingCurrent.LastManualSyncDate,
                LastSyncFinishDate = request.LastSyncFinishDate ?? syncSettingCurrent.LastSyncFinishDate,
                LastSyncStartDate = request.LastSyncStartDate ?? syncSettingCurrent.LastSyncStartDate,
                PartyId = request.PartyId,
                PreviousSyncStartDate = request.PreviousSyncStartDate ?? syncSettingCurrent.PreviousSyncStartDate,
                RemotePartyId = request.RemotePartyId ?? syncSettingCurrent.RemotePartyId,
                ContactSyncStatus = request.SyncStatus ?? syncSettingCurrent.ContactSyncStatus
            };

            // true if updated, false if not found or not modified (tracked entities)
            await _sqlConnection.UpdateAsync(syncSetting);

            return new SyncSettingModel
            {
                DailySynced = syncSetting.DailySynced,
                IntegrationType = syncSetting.IntegrationType,
                LastManualSyncDate = syncSetting.LastManualSyncDate,
                LastSyncFinishDate = syncSetting.LastSyncFinishDate,
                LastSyncStartDate = syncSetting.LastSyncStartDate,
                PartyId = syncSetting.PartyId,
                PreviousSyncStartDate = syncSetting.PreviousSyncStartDate,
                RemotePartyId = syncSetting.RemotePartyId,
                SyncStatus = syncSetting.ContactSyncStatus
            };
        }
    }
}
