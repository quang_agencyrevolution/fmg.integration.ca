﻿using System;
using FMG.Integration.CA.Application.SyncSetting.Models;
using FMG.Integration.CA.Domain.Enumerations;
using MediatR;

namespace FMG.Integration.CA.Application.SyncSetting.Commands.UpdateSyncSetting
{
    public class UpdateSyncSettingCommand : IRequest<SyncSettingModel>
    {
        public int PartyId { get; set; }

        public CRMIntegrationType? IntegrationType { get; set; }

        public bool? DailySynced { get; set; }

        /// <summary>
        /// The status of process sync contacts with Salesforce
        ///     Syncing
        ///     SyncedSuccess
        ///     SyncedError
        /// </summary>
        public SyncStatus? SyncStatus { get; set; }

        public DateTime? LastSyncFinishDate { get; set; }

        /// <summary>
        /// The datetime when user click on "Update Contacts" to force sync
        /// </summary>
        public DateTime? LastManualSyncDate { get; set; }

        /// <summary>
        /// The starting time (the time the sync status changed to Syncing) of the current/last sync
        /// </summary>
        public DateTime? LastSyncStartDate { get; set; }

        /// <summary>
        /// The starting time (the time the sync status changed to Syncing) of the previous (the second-last) sync
        /// </summary>
        public DateTime? PreviousSyncStartDate { get; set; }

        /// <summary>
        /// The connected PartyId in the 3rd party system
        /// </summary>
        public string RemotePartyId { get; set; }
    }
}
