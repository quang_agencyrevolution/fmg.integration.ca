﻿using System.Data.SqlClient;
using System.Threading;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Application.SyncSetting.Models;
using FMG.Integration.CA.Domain.Entities;
using MediatR;

namespace FMG.Integration.CA.Application.SyncSetting.Commands.CreateSyncSetting
{
    public class CreateSyncSettingCommandHandler :
        IRequestHandler<CreateSyncSettingCommand, SyncSettingModel>
    {
        private readonly SqlConnection _sqlConnection;

        public CreateSyncSettingCommandHandler(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public async Task<SyncSettingModel> Handle(CreateSyncSettingCommand request, CancellationToken cancellationToken)
        {
            // Identity of inserted entity: this is incorrect when we specify key (PartyId)
            await _sqlConnection.InsertAsync(new SyncSettingEntity
            {
                DailySynced = request.DailySynced,
                IntegrationType = request.IntegrationType,
                LastManualSyncDate = request.LastManualSyncDate,
                LastSyncFinishDate = request.LastSyncFinishDate,
                LastSyncStartDate = request.LastSyncStartDate,
                PartyId = request.PartyId,
                PreviousSyncStartDate = request.PreviousSyncStartDate,
                RemotePartyId = request.RemotePartyId,
                ContactSyncStatus = request.SyncStatus
            });

            return new SyncSettingModel
            {
                DailySynced = request.DailySynced,
                IntegrationType = request.IntegrationType,
                LastManualSyncDate = request.LastManualSyncDate,
                LastSyncFinishDate = request.LastSyncFinishDate,
                LastSyncStartDate = request.LastSyncStartDate,
                PartyId = request.PartyId,
                PreviousSyncStartDate = request.PreviousSyncStartDate,
                RemotePartyId = request.RemotePartyId,
                SyncStatus = request.SyncStatus
            };
        }
    }
}
