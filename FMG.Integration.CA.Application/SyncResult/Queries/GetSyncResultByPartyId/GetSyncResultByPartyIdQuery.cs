﻿namespace FMG.Integration.CA.Application.SyncResult.Queries.GetSyncResultByPartyId
{
    public class GetSyncResultByPartyIdQuery
    {
        public int PartyId { get; set; }
    }
}
