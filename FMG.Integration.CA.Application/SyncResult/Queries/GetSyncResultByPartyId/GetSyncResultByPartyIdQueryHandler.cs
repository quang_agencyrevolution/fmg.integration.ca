﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Application.Exceptions;
using FMG.Integration.CA.Application.SyncResult.Models;
using FMG.Integration.CA.Domain.Entities;

namespace FMG.Integration.CA.Application.SyncResult.Queries.GetSyncResultByPartyId
{
    public class GetSyncResultByPartyIdQueryHandler
    {
        private readonly SqlConnection _sqlConnection;

        public GetSyncResultByPartyIdQueryHandler(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public async Task<SyncResultModel> Handle(GetSyncResultByPartyIdQuery request)
        {
            var syncResult = await _sqlConnection.GetAsync<SyncResultEntity>(request.PartyId);

            if (syncResult == null)
                throw new NotFoundException($"Not found SyncResult for PartyId = {request.PartyId}");

            return new SyncResultModel
            {
                Error = syncResult.Error,
                FMGContactId = syncResult.FMGContactId,
                LastModifiedDate = syncResult.LastModifiedDate,
                PartyId = syncResult.PartyId,
                RemoteContactId = syncResult.RemoteContactId,
                RemoteContactName = syncResult.RemoteContactName,
                RemoteEmailAddress = syncResult.RemoteEmailAddress,
                SyncStatus = syncResult.SyncStatus
            };
        }

    }
}
