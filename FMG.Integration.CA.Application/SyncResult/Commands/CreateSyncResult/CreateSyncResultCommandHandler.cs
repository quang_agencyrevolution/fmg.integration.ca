﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Application.SyncResult.Models;
using FMG.Integration.CA.Domain.Entities;

namespace FMG.Integration.CA.Application.SyncResult.Commands.CreateSyncResult
{
    public class CreateSyncResultCommandHandler
    {
        private readonly SqlConnection _sqlConnection;

        public CreateSyncResultCommandHandler(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public async Task<SyncResultModel> Handle(CreateSyncResultCommand request)
        {
            // Identity of inserted entity
            var partyId = await _sqlConnection.InsertAsync(new SyncResultEntity
            {
                Error = request.Error,
                FMGContactId = request.FMGContactId,
                LastModifiedDate = request.LastModifiedDate,
                PartyId = request.PartyId,
                RemoteContactId = request.RemoteContactId,
                RemoteContactName = request.RemoteContactName,
                RemoteEmailAddress = request.RemoteEmailAddress,
                SyncStatus = request.SyncStatus
            });

            if (partyId == 0) return null;

            return new SyncResultModel
            {
                Error = request.Error,
                FMGContactId = request.FMGContactId,
                LastModifiedDate = request.LastModifiedDate,
                PartyId = request.PartyId,
                RemoteContactId = request.RemoteContactId,
                RemoteContactName = request.RemoteContactName,
                RemoteEmailAddress = request.RemoteEmailAddress,
                SyncStatus = request.SyncStatus
            };
        }
    }
}
