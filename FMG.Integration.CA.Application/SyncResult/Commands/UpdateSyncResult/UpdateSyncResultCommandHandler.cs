﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Application.Exceptions;
using FMG.Integration.CA.Application.SyncResult.Models;
using FMG.Integration.CA.Domain.Entities;

namespace FMG.Integration.CA.Application.SyncResult.Commands.UpdateSyncResult
{
    public class UpdateSyncResultCommandHandler
    {
        private readonly SqlConnection _sqlConnection;

        public UpdateSyncResultCommandHandler(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public async Task<SyncResultModel> Handle(UpdateSyncResultCommand request)
        {
            var syncResultCurrent = await _sqlConnection.GetAsync<SyncResultEntity>(request.PartyId);

            if (syncResultCurrent == null)
            {
                throw new NotFoundException($"Not found SyncResult for PartyId = {request.PartyId}");
            }

            var syncResult = new SyncResultEntity
            {
                Error = request.Error ?? syncResultCurrent.Error,
                FMGContactId = request.FMGContactId ?? syncResultCurrent.FMGContactId,
                PartyId = request.PartyId,
                RemoteContactId = request.RemoteContactId ?? syncResultCurrent.RemoteContactId,
                RemoteContactName = request.RemoteContactName ?? syncResultCurrent.RemoteContactName,
                RemoteEmailAddress = request.RemoteEmailAddress ?? syncResultCurrent.RemoteEmailAddress,
                SyncStatus = request.SyncStatus ?? syncResultCurrent.SyncStatus,
            };

            // true if updated, false if not found or not modified (tracked entities)
            await _sqlConnection.UpdateAsync(syncResult);

            return new SyncResultModel
            {
                Error = syncResult.Error,
                FMGContactId = syncResult.FMGContactId,
                LastModifiedDate = syncResult.LastModifiedDate,
                PartyId = syncResult.PartyId,
                RemoteContactId = syncResult.RemoteContactId,
                RemoteContactName = syncResult.RemoteContactName,
                RemoteEmailAddress = syncResult.RemoteEmailAddress,
                SyncStatus = syncResult.SyncStatus
            };
        }
    }
}
