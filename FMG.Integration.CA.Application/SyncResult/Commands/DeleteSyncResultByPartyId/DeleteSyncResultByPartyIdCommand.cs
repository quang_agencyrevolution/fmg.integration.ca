﻿namespace FMG.Integration.CA.Application.SyncResult.Commands.DeleteSyncResultByPartyId
{
    public class DeleteSyncResultByPartyIdCommand
    {
        public int PartyId { get; set; }
    }
}
