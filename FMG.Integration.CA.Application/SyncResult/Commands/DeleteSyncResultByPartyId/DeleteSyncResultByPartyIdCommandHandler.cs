﻿using System.Data.SqlClient;
using System.Threading.Tasks;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Domain.Entities;

namespace FMG.Integration.CA.Application.SyncResult.Commands.DeleteSyncResultByPartyId
{
    public class DeleteSyncResultByPartyIdCommandHandler
    {
        private readonly SqlConnection _sqlConnection;

        public DeleteSyncResultByPartyIdCommandHandler(SqlConnection sqlConnection)
        {
            _sqlConnection = sqlConnection;
        }

        public async Task Handle(DeleteSyncResultByPartyIdCommand request)
        {
            // true if deleted, false if not found
            await _sqlConnection.DeleteAsync(new SyncResultEntity
            {
                PartyId = request.PartyId
            });
        }
    }
}
