﻿namespace FMG.Integration.CA.Application.Interfaces.Enumerations
{
    public enum LogLevel
    {
        Verbose = 0,
        Debug = 1,
        Info = 2,
        Warning = 3,
        Error = 4,
        Fatal = 5
    }
}
