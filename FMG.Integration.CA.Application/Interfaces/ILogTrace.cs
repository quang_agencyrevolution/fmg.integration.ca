﻿using System;
using FMG.Integration.CA.Application.Interfaces.Enumerations;

namespace FMG.Integration.CA.Application.Interfaces
{
    public interface ILogTrace
    {
        void Add(LogLevel level, string domain, object message, DateTime? prevTime = null);

        void AddError(Exception ex, DateTime? prevTime = null);

        void Flush();
    }
}
