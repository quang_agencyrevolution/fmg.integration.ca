﻿using System.Collections.Generic;
using System.Data.SqlClient;
using Dapper.Contrib.Extensions;
using FMG.Integration.CA.Domain.Entities;

namespace FMG.Integration.CA.Persistence
{
    public class DBInitializer
    {
        static readonly Dictionary<string, SqlConnection> connectionDict = new Dictionary<string, SqlConnection>();
        public static SqlConnection Initialize(string connectionString)
        {
            if (!connectionDict.ContainsKey(connectionString))
            {
                connectionDict.Add(connectionString, new SqlConnection(connectionString));
            }

            TableNameMap();

            return connectionDict[connectionString];
        }

        static void TableNameMap()
        {
            var tableDict = new Dictionary<string, string>
            {
                { typeof(SyncSettingEntity).Name, "CRMSyncSettings" },
                { typeof(SyncResultEntity).Name, "CRMSyncResults" }
            };

            SqlMapperExtensions.TableNameMapper = (type) =>
            {
                if (tableDict.ContainsKey(type.Name)) return tableDict[type.Name];

                return type.Name;
            };

        }
    }
}
