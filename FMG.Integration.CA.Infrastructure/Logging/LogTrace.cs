﻿using System;
using System.Collections.Generic;
using FMG.Integration.CA.Application.Interfaces;
using FMG.Integration.CA.Application.Interfaces.Enumerations;

namespace FMG.Integration.CA.Infrastructure.Logging
{
    public class LogTrace : ILogTrace
    {
        protected IList<LogEntry> _entries = new List<LogEntry>();
        protected object _lock = new object();

        public void Add(LogLevel level, string domain, object message, DateTime? prevTime = null)
        {
            lock (_lock)
            {
                // implement time elapsed
                double? timeElapsed = null;
                if (prevTime != null)
                {
                    var now = DateTime.UtcNow;
                    timeElapsed = now.Subtract(prevTime.Value).TotalMilliseconds;
                }

                // Avoid self referencing loop detected for property when Json serializer
                var logMsg = message;
                if (message is Exception ex)
                {
                    logMsg = new { ex.Message, ex.StackTrace };
                }

                var logEntry = new LogEntry
                {
                    Level = level,
                    Domain = domain,
                    Message = logMsg,
                    TimeElapsed = timeElapsed
                };

                Console.WriteLine($"{logEntry.Domain}");
                _entries.Add(logEntry);
            }
        }

        public void AddError(Exception ex, DateTime? prevTime = null)
        {
            lock (_lock)
            {
                Add(LogLevel.Error, ex.Message, ex.StackTrace, prevTime);
            }
        }

        public void Flush()
        {

        }
    }
}
