﻿using FMG.Integration.CA.Application.Interfaces.Enumerations;

namespace FMG.Integration.CA.Infrastructure.Logging
{
    public class LogEntry
    {
        /// <summary>
        /// Gets or sets the level.
        /// </summary>
        /// <value>The level.</value>
        //[JsonConverter(typeof(StringEnumConverter))]
        public LogLevel Level { get; set; }

        /// <summary>
        /// Gets or sets the domain.
        /// </summary>
        /// <value>The domain.</value>
        public string Domain { get; set; }

        /// <summary>
        /// Gets or sets the message.
        /// </summary>
        /// <value>The message.</value>
        public object Message { get; set; }

        /// <summary>
        /// Gets or sets the time elapsed.
        /// </summary>
        /// <value>The time elapsed.</value>
        public double? TimeElapsed { get; set; }
    }
}
