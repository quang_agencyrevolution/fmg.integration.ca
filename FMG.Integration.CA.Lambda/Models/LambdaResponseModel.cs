﻿using System.Net;

namespace FMG.Integration.CA.Lambda.Models
{
    public class LambdaResponseModel
    {
        public LambdaResponseModel()
        {
            StatusCode = HttpStatusCode.OK;
            Data = new object();
        }

        public HttpStatusCode StatusCode { get; set; }
        public object Data { get; set; }
    }
}
