﻿namespace FMG.Integration.CA.Lambda.Models
{
    public class LambdaRequestModel
    {
        public string ActionType { get; set; }

        public string Data { get; set; }
    }
}
