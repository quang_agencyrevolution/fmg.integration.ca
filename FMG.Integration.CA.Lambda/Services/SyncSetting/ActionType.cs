﻿namespace FMG.Integration.CA.Lambda.Services.SyncSetting
{
    public class ActionType
    {
        public const string GET_SYNC_SETTING_BY_PARTY_ID = "GET_SYNC_SETTING_BY_PARTY_ID";
        public const string CREATE_SYNC_SETTING = "CREATE_SYNC_SETTING";
        public const string DELETE_SYNC_SETTING_BY_PARTY_ID = "DELETE_SYNC_SETTING_BY_PARTY_ID";
        public const string UPDATE_SYNC_SETTING = "UPDATE_SYNC_SETTING";
    }
}
