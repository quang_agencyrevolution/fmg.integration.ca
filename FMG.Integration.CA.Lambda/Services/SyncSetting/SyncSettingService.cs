﻿using System.Threading;
using System.Threading.Tasks;
using Amazon.Lambda.Core;
using FMG.Integration.CA.Application.SyncSetting.Commands.CreateSyncSetting;
using FMG.Integration.CA.Application.SyncSetting.Commands.DeleteSyncSettingByPartyId;
using FMG.Integration.CA.Application.SyncSetting.Commands.UpdateSyncSetting;
using FMG.Integration.CA.Application.SyncSetting.Queries.GetSyncSettingByPartyId;
using FMG.Integration.CA.Lambda.Models;
using FMG.Integration.CA.Persistence;
using Newtonsoft.Json;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializer(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace FMG.Integration.CA.Lambda.Services.SyncSetting
{
    public class SyncSettingService
    {
        private readonly GetSyncSettingByPartyIdQueryHandler _getSyncSettingByPartyIdQueryHandler;
        private readonly CreateSyncSettingCommandHandler _createSyncSettingCommandHandler;
        private readonly UpdateSyncSettingCommandHandler _updateSyncSettingCommandHandler;
        private readonly DeleteSyncSettingByPartyIdCommandHandler _deleteSyncSettingByPartyIdCommandHandler;

        public SyncSettingService()
        {
            var connection = DBInitializer.Initialize("Data Source=.; Initial Catalog=CMS;Persist Security Info=True;User ID=ci;Password=ci;MultipleActiveResultSets=True");
            _getSyncSettingByPartyIdQueryHandler = new GetSyncSettingByPartyIdQueryHandler(connection);
            _createSyncSettingCommandHandler = new CreateSyncSettingCommandHandler(connection);
            _updateSyncSettingCommandHandler = new UpdateSyncSettingCommandHandler(connection);
            _deleteSyncSettingByPartyIdCommandHandler = new DeleteSyncSettingByPartyIdCommandHandler(connection);
        }

        public async Task<LambdaResponseModel> HandleAsync(
            LambdaRequestModel request, ILambdaContext context)
        {
            var reponse = new LambdaResponseModel();

            switch (request.ActionType)
            {
                case ActionType.GET_SYNC_SETTING_BY_PARTY_ID:
                    var query = JsonConvert.DeserializeObject<GetSyncSettingByPartyIdQuery>(request.Data);
                    reponse.Data = await _getSyncSettingByPartyIdQueryHandler.Handle(query, new CancellationToken());
                    break;
                case ActionType.CREATE_SYNC_SETTING:
                    var createCommand = JsonConvert.DeserializeObject<CreateSyncSettingCommand>(request.Data);
                    reponse.Data = await _createSyncSettingCommandHandler.Handle(createCommand, new CancellationToken());
                    break;
                case ActionType.UPDATE_SYNC_SETTING:
                    var updateCommand = JsonConvert.DeserializeObject<UpdateSyncSettingCommand>(request.Data);
                    reponse.Data = await _updateSyncSettingCommandHandler.Handle(updateCommand, new CancellationToken());
                    break;
                case ActionType.DELETE_SYNC_SETTING_BY_PARTY_ID:
                    var deleteCommand = JsonConvert.DeserializeObject<DeleteSyncSettingByPartyIdCommand>(request.Data);
                    await _deleteSyncSettingByPartyIdCommandHandler.Handle(deleteCommand, new CancellationToken());
                    break;
            }

            return reponse;
        }
    }
}
