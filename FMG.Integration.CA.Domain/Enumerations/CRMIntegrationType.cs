﻿namespace FMG.Integration.CA.Domain.Enumerations
{
    public enum CRMIntegrationType
    {
        Undefined = 0,
        Redtail = 1,
        Salesforce = 2,
        MarketingPro = 3,
        Wealthbox = 4,
        Ebix = 5
    }
}
