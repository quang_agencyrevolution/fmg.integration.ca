﻿namespace FMG.Integration.CA.Domain.Enumerations
{
    public enum SyncStatus
    {
        // User has already logged into Partner system but has not finished all the setting steps
        SettingUp = 0,

        // User has already logged in and completed all the setting steps. The system is currently syncing
        // Contacts/Groups from Partner system
        Syncing = 1,

        // User has already logged in and completed all the setting steps. The system is currently free,
        // doesn't do any sync job from Partner system
        Integrated = 2
    }
}
