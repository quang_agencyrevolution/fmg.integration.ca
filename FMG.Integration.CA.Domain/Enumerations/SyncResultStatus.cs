﻿namespace FMG.Integration.CA.Domain.Enumerations
{
    public enum SyncResultStatus
    {
        Synced = 0,
        Error = 1,
        NotSynced = 2
    }
}
