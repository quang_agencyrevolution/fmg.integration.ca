﻿using System;

namespace FMG.Integration.CA.Domain.Entities
{
    public class OAuthToken
    {
        public int PartyId { get; set; }

        public string AccessToken { get; set; }

        public string RefreshToken { get; set; }

        public DateTime CreatedAt { get; set; }

        public DateTime? ExpiredAt { get; set; }

        /// <summary>
        /// Examples 
        /// https://na50.salesforce.com
        /// https://ap5.salesforce.com
        /// </summary>
        public string APIEndpoint { get; set; }

        /// <summary>
        /// The last time we push form submit to external CRM system
        /// </summary>
        public DateTime? LastPushDate { get; set; }
    }
}
