﻿using System;
using FMG.Integration.CA.Domain.Enumerations;

namespace FMG.Integration.CA.Domain.Entities
{
    public class SyncResultEntity
    {
        public int PartyId { get; set; }
        public string RemoteContactId { get; set; }
        public string RemoteContactName { get; set; }
        public string RemoteEmailAddress { get; set; }
        public int? FMGContactId { get; set; }
        public SyncResultStatus SyncStatus { get; set; }
        public DateTime LastModifiedDate { get; set; }
        public string Error { get; set; }
    }
}
