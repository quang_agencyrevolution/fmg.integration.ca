﻿using System;
using System.Threading.Tasks;
using FMG.Integration.CA.Application.Interfaces;
using FMG.Integration.CA.Application.SyncSetting.Commands.CreateSyncSetting;
using FMG.Integration.CA.Application.SyncSetting.Commands.DeleteSyncSettingByPartyId;
using FMG.Integration.CA.Application.SyncSetting.Queries.GetSyncSettingByPartyId;
using FMG.Integration.CA.Domain.Enumerations;
using FMG.Integration.CA.Infrastructure.Logging;
using FMG.Integration.CA.Persistence;
using MediatR;
using Microsoft.Extensions.DependencyInjection;

namespace FMG.Integration.CA.ConsoleApp
{
    class Program
    {
        static readonly string connectionString = "Data Source=.;Initial Catalog=CMS;Persist Security Info=True;User ID=ci;Password=ci;MultipleActiveResultSets=true;";
        static int partyId => 1234;

        static void Main(string[] args)
        {
            Console.WriteLine("Hello World!");
            var mediator = BuildMediator();
            Task.WaitAll(GetSyncSettingByPartyId(mediator));
            Console.ReadLine();
        }

        static async Task GetSyncSettingByPartyId(IMediator mediator)
        {
            await mediator.Send(new DeleteSyncSettingByPartyIdCommand { PartyId = partyId });

            var syncSetting = await mediator.Send(new CreateSyncSettingCommand
            {
                PartyId = partyId,
                DailySynced = true,
                IntegrationType = CRMIntegrationType.Salesforce,
                SyncStatus = SyncStatus.SettingUp
            });

            //syncSetting = await mediator.Send(new GetSyncSettingByPartyIdQuery
            //{
            //    PartyId = partyId
            //});

            //await mediator.Send(new DeleteSyncSettingByPartyIdCommand
            //{
            //    PartyId = partyId
            //});
            //Console.WriteLine($"{syncSetting.PartyId}");
        }

        private static IMediator BuildMediator()
        {
            var services = new ServiceCollection();

            services.AddScoped<ServiceFactory>(p => p.GetService);

            var sqlConnection = DBInitializer.Initialize(connectionString);
            //var handler = new CreateSyncSettingCommandHandler(sqlConnection);
            //Task.WaitAll(handler.Handle(new CreateSyncSettingCommand
            //{
            //    PartyId = partyId,
            //    DailySynced = true,
            //    IntegrationType = CRMIntegrationType.Salesforce,
            //    SyncStatus = SyncStatus.SettingUp
            //}, new System.Threading.CancellationToken()));

            services.AddSingleton(sqlConnection);

            services.AddSingleton(typeof(ILogTrace), new LogTrace());

            //Pipeline
            //services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            //services.AddScoped(typeof(IPipelineBehavior<,>), typeof(RequestPostProcessorBehavior<,>));
            //services.AddScoped(typeof(IPipelineBehavior<,>), typeof(GenericPipelineBehavior<,>));
            //services.AddScoped(typeof(IRequestPreProcessor<>), typeof(GenericRequestPreProcessor<>));
            //services.AddScoped(typeof(IRequestPostProcessor<,>), typeof(GenericRequestPostProcessor<,>));

            //This causes a type load exception. https://github.com/jbogard/MediatR.Extensions.Microsoft.DependencyInjection/issues/12
            //services.AddScoped(typeof(IRequestPostProcessor<,>), typeof(ConstrainedRequestPostProcessor<,>));
            //services.AddScoped(typeof(INotificationHandler<>), typeof(ConstrainedPingedHandler<>));

            // Use Scrutor to scan and register all
            // classes as their implemented interfaces.
            services.Scan(scan => scan
                .FromAssembliesOf(typeof(IMediator), typeof(GetSyncSettingByPartyIdQuery))
                .AddClasses()
                .AsImplementedInterfaces());

            var provider = services.BuildServiceProvider();

            return provider.GetRequiredService<IMediator>();
        }
    }
}
